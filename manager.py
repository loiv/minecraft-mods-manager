import logging
import os
import subprocess
import sys
from multiprocessing import Lock

import git
from aiohttp import web
from aiohttp.web import Request, Response
from dotenv import load_dotenv

lock = Lock()

FORMATTER = logging.Formatter(
    "%(asctime)s.%(msecs)03d %(levelname)s %(module)s - "
    "%(funcName)s: %(message)s"
)


def get_logger(name: str) -> logging.Logger:
    logger = logging.getLogger(name)

    logger.setLevel(logging.INFO)

    console_handler = logging.StreamHandler(sys.stdout)
    console_handler.setFormatter(FORMATTER)
    logger.addHandler(console_handler)

    logger.propagate = False

    return logger


logger = get_logger(__name__)


def save_error(message: str) -> None:
    error_file_path = "error"
    with open(error_file_path, "a") as f:
        f.write(message)


def git_pull(repo_path: str) -> None:
    try:
        repo = git.Repo(repo_path)
    except git.exc.NoSuchPathError:
        save_error(f"Brak ścieżki {repo_path}")
        return
    try:
        repo.remote(name="origin").pull()
    except git.exc.GitCommandError as ex:
        save_error(str(ex))
        return


async def update(request: Request) -> Response:
    if os.path.isfile("./error"):
        return web.Response(
            status=409,
            text="Wcześniej wystąpił błąd. Poproś o ręczną aktualizację",
        )
    if not lock.acquire(block=False):
        return web.Response(status=200, text="Ktoś już aktualizuje pliki")

    response = web.Response(status=202, text="Pliki zostaną zaktualizowane")
    await response.prepare(request)
    await response.write_eof()

    git_pull(request.app["REPO_PATH"])

    lock.release()
    return response


async def status(request: Request) -> Response:
    repo = git.Repo(request.app["REPO_PATH"])
    commit_hexsha = repo.head.commit.hexsha
    message = f"HEAD: {repo.active_branch.name} {commit_hexsha}"

    if os.path.isfile("./error"):
        with open("./error", "r") as f:
            lines = f.readlines()
            if lines:
                message += "\n\n"
                message += "".join(lines)

    return web.Response(status=200, text=message)


async def restart(request: Request) -> Response:
    data = await request.post()
    stripped_password = data.get("password").strip()
    if not stripped_password:
        return web.Response(status=422, text="Brak pola 'password'")

    if stripped_password != request.app["PASSWORD"]:
        return web.Response(status=401, text="Niepoprawne hasło")

    response = web.Response(status=200, text="Serwer zostanie zrestartowany")
    await response.prepare(request)
    await response.write_eof()

    subprocess.run(request.app["CMD_RESTART"])
    return response


async def health(request: Request):
    return web.Response(status=200)


async def on_startup(app) -> None:
    basedir = os.path.abspath(os.path.dirname(__file__))
    load_dotenv(os.path.join(basedir, ".env"))

    CMD_RESTART = ["systemctl", "--user", "restart"]

    SERVICE_NAME = os.environ.get("SERVICE_NAME", "").strip()
    if not SERVICE_NAME:
        logger.error("No service name specified!")
        sys.exit(4)
    CMD_RESTART.append(SERVICE_NAME)
    app["CMD_RESTART"] = CMD_RESTART

    PASSWORD = os.environ.get("PASSWORD", "").strip()
    if not PASSWORD:
        logger.error("Password not specified!")
        sys.exit(4)
    app["PASSWORD"] = PASSWORD

    REPO_PATH = os.environ.get("REPO_PATH", "").strip()
    if not REPO_PATH:
        logger.error("Repository to update path not specified!")
        sys.exit(4)
    app["REPO_PATH"] = REPO_PATH


async def my_web_app() -> web.Application:
    app = web.Application()
    app.add_routes(
        [
            web.get("/", status),
            web.get("/status", status),
            web.post("/update", update),
            web.post("/restart", restart),
            web.get("/health", health),
        ]
    )

    app.on_startup.append(on_startup)

    return app
