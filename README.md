# Minecraft Mods Updater

## Pulling with fast-forward option

Git should be configured with _fast-forward_ option
with `git config --local merge.ff only` to ensure that there won't be any conflicts.
